package mx.com.ids.apitest.controller;

import java.util.List;
import java.util.UUID;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import mx.com.ids.apitest.model.Person;
import mx.com.ids.apitest.repository.PersonRepository;

@RestController
@RequestMapping("/api")
public class PersonController {

    @Autowired
    private PersonRepository repository;

    @GetMapping("/persons")
    public List<Person> allPersons() {
        return repository.findAll( Sort.by(Sort.Direction.ASC, "name") );
    }

    @GetMapping("/person/{name}")
    public List<Person> findByName(@PathVariable("name") String name) {
        return repository.findByName(name);
    }

    @PostMapping("/person")
    public Person createPerson(@RequestBody Person person) {
        return repository.save(person);
    }

    @PutMapping("/person/{id}")
    public Person updatePerson(@PathVariable int id, @RequestBody Person person) {
        return repository.save(person);
    }

    @DeleteMapping("/person/{id}")
    public void deletePerson(@PathVariable("id") Long id) {
        repository.deleteById(id);
    }

    @GetMapping("/personLogin/{userName}")
    public String findByNameLogin(@PathVariable("userName") String userName) {
        String message;

        List<Person> lstPerson = repository.findByUserName(userName);
        System.out.println( "lstPerson: " + lstPerson);

        if( lstPerson != null && lstPerson.size() > 0 ) {
            Person person = lstPerson.get(0);

            if( person.getStatus() ) {
                message = person.getName() + " ya te encuentras autenticado...";
            } else {
                // Genera cc¿adena aleatoria UUID
                UUID uuid = UUID.randomUUID();
                String uuidAsString = uuid.toString();
                System.out.println("Your UUID is: " + uuidAsString + ", size: " + uuidAsString.length());

                // Asignamos valores a actualizar
                person.setUuid(uuid.toString());
                person.setStatus(true);
                repository.save(person);

                message = person.getName() + " te has autenticado correctamente...";
            }
        } else {
            message = "Usuario inexistente...";
        }

        return message;
    }

    @GetMapping("/personLogout/{userName}")
    public String findByNameLogout(@PathVariable("userName") String userName) {
        String message;

        List<Person> lstPerson = repository.findByUserName(userName);
        System.out.println( "lstPerson: " + lstPerson);

        if( lstPerson != null && lstPerson.size() > 0 ) {
            Person person = lstPerson.get(0);

            // Asignamos valores a actualizar
            person.setUuid(null);
            person.setStatus(false);
            repository.save(person);

            message = person.getName() + ", logout exitoso, vuelve pronto...";
        } else {
            message = "Usuario inexistente...";
        }

        return message;
    }
}
